const scanner = require("sonarqube-scanner");

scanner(
  {
    serverUrl: "http://localhost:9000",
    token: "sqa_4f742e07d9265e57b22ae4abdfc9e43b0c07aa3e",
    exclusions: "**/*.test.tsx",
    tests: "./src",
    test: {
      inclusions: "**/*.test.tsx,**/*.test.ts",
    },
    typescript: {
      lcov: {
        reportPaths: "coverage/lcov.info",
      },
    },
    testExecutionReportPaths: "test-report.xml",
    options: {
      "sonar.sources": "./src",
    },
  },
  () => process.exit()
);
